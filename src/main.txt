#include <Arduino.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <NativeEthernet.h>
#include <NativeEthernetUdp.h>

byte mac[] = { //set mac address
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};

IPAddress ip(10, 0, 0, 3); //set local IP

unsigned int localPort = 8888; //UDP port for listening

uint8_t tx_packet_buffer[UDP_TX_PACKET_MAX_SIZE]; //buffer for sent packet
uint8_t rx_packet_buffer[UDP_TX_PACKET_MAX_SIZE]; //buffer for receiving packet

EthernetUDP Udp;

void setup() {
//Start the ethernet
Ethernet.begin(mac, ip);
//open serial comms for debugging (might remove after udp working)
Serial.begin(115200);
while(!Serial){

}

if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  else{
    Serial.println("Ethernet Hardware Found");
  }

  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }
  else{
    Serial.println(Ethernet.localIP());
  }


Udp.begin(localPort);

}

void loop() {

  //placeholder loop code

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i=0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(tx_packet_buffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(tx_packet_buffer);

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(rx_packet_buffer);
    Udp.endPacket();
  }
  delay(10);
}